from flask import Flask, make_response, redirect, request

from processor import LinksHandler

HOSTNAME = 'http://localhost:5000'

app = Flask(__name__)

links_handler = LinksHandler()


@app.route('/urls', methods=['POST'])
def set_url():
    long_link = request.json.get('long_link')
    if long_link is None:
        response = make_response({'message': 'Please, provide `long_link` in the body'}, 400)
    else:
        short_link = links_handler.set_short_link(long_link)
        response = {'short_link': f'{HOSTNAME}/urls/{short_link}'}
    return response


@app.route('/urls/<short_code>', methods=['GET'])
def get_link(short_code):
    links_handler.increase_link_count(short_code)
    long_link = links_handler.get_link(short_code)
    if long_link:
        response = redirect(long_link, code=302)
    else:
        # No default redirect is defined in the task so I return 404 error
        response = make_response({'message': 'Link was not found'}, 404)
    return response


@app.route('/urls/<short_code>', methods=['DELETE'])
def delete_link(short_code):
    links_handler.delete_link(short_code)
    return {'message': 'ok'}


@app.route('/urls/<short_code>', methods=['PUT'])
def update_link(short_code):
    long_link = request.json.get('long_link')
    if long_link is None:
        response = make_response({'message': 'Please, provide `long_link` in the body'}, 400)
    else:
        short_link = links_handler.update_link(short_code, long_link)
        response = {'short_link': f'{HOSTNAME}/urls/{short_link}'}
    return response


@app.route('/urls/<short_code>/stats', methods=['GET'])
def get_link_stats(short_code):
    link_stats = links_handler.get_link_stats(short_code)
    return {'num_24hour_visits': link_stats}
