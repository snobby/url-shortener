## Url shortener service
# Installation
Tested on python 3.8
```bash
pip3 install -r requirements.txt
```

# Usage
Run server
```bash
export FLASK_APP=server
flask run
```

Requests
```bash
# Create link
curl --request POST \
  --url http://127.0.0.1:5000/urls \
  --header 'Content-Type: application/json' \
  --data '{"long_link": "https://tattered-iguanodon-14d.notion.site/getmatch-173881b974a94062a50ad7157a1e6b5d"}'

# Get link
curl --request GET \
  --url http://127.0.0.1:5000/urls/<short_code> \
  --header 'Content-Type: application/json'

# Delete link
curl --request DELETE \
  --url http://127.0.0.1:5000/urls/<short_code>

# Update link
curl --request PUT \
  --url http://127.0.0.1:5000/urls/<short_code> \
  --header 'Content-Type: application/json' \
  --data '{"long_link": "https://google.com"}'

# Get stats
curl --request GET \
  --url http://127.0.0.1:5000/urls/<short_code>/stats

```