import uuid
from datetime import datetime, timedelta, timezone


class LinksHandler:
    links_match = dict()

    def set_short_link(self, long_link: str) -> str:
        """
        Number of truncated chars depends on the collision safety that we want to achieve
        There are better methods (including capital chars for example) that we can use than just uuid4 truncation
        But this one is fast and simple
        """
        short_link = uuid.uuid4().hex[:8]
        self.links_match[short_link] = {
            'long_link': long_link,
            'datetime_visited': []
        }
        return short_link

    def update_link(self, short_code: str, long_link: str) -> str:
        """
        I reset the stats for the link on update
        """
        self.links_match[short_code] = {
            'long_link': long_link,
            'datetime_visited': []
        }

        return short_code

    def increase_link_count(self, short_code: str):
        """
        In order to found visits for the last 24h I append visited datetime
        """
        short_link = self.links_match.get(short_code)
        if short_link is not None:
            short_link['datetime_visited'].append(datetime.now(timezone.utc))

    def get_link(self, short_code: str) -> str:
        """
        If link is not found I return 404
        """
        short_link = self.links_match.get(short_code)
        if short_link is None:
            long_link = ''
        else:
            long_link = short_link['long_link']
        return long_link

    def delete_link(self, short_code: str):
        """
        If link is not found I do nothing
        """
        self.links_match.pop(short_code, '')

    def get_link_stats(self, short_code: str) -> int:
        short_link = self.links_match.get(short_code)
        link_stats = 0
        if short_link is not None:
            datetime_24hours_before = datetime.now(timezone.utc) - timedelta(days=1)
            for datetime_visited in short_link['datetime_visited'][::-1]:
                if datetime_visited > datetime_24hours_before:
                    link_stats += 1
                else:
                    break
        return link_stats
